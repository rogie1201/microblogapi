<?php
declare (strict_types = 1);

namespace Api;

use Cake\Core\BasePlugin;
use Cake\Core\PluginApplicationInterface;
use Cake\Http\MiddlewareQueue;
use Cake\Routing\RouteBuilder;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Plugin for Api
 */
class Plugin extends BasePlugin
{
    /**
     * Load all the plugin configuration and bootstrap logic.
     *
     * The host application is provided as an argument. This allows you to load
     * additional plugin dependencies, or attach events.
     *
     * @param \Cake\Core\PluginApplicationInterface $app The host application
     * @return void
     */
    public function bootstrap(PluginApplicationInterface $app): void
    {
    }

    /**
     * Add routes for the plugin.
     *
     * If your plugin has many routes and you would like to isolate them into a separate file,
     * you can create `$plugin/config/routes.php` and delete this method.
     *
     * @param \Cake\Routing\RouteBuilder $routes The route builder to update.
     * @return void
     */
    public function routes(RouteBuilder $routes): void
    {
        $routes->plugin(
            'Api',
            ['path' => '/api'],
            function (RouteBuilder $builder) {
                // Add custom routes here

                // API ver 1.0 routes
                // add new block for more prefixes
                $builder->prefix('V1', ['path' => '/v1'], function (RouteBuilder $routeBuilder) {
                    $routeBuilder->setExtensions(['json']); // allow extensins. this have to be here for each prefix

                    //$routeBuilder->connect('/users/add', ['controller' => 'Users', 'action' => 'login']);

                    $routeBuilder->fallbacks();
                });

                $builder->fallbacks();
            }
        );

        parent::routes($routes);
    }

    /**
     * Add middleware for the plugin.
     *
     * @param \Cake\Http\MiddlewareQueue $middleware The middleware queue to update.
     * @return \Cake\Http\MiddlewareQueue
     */
    public function middleware(MiddlewareQueue $middlewareQueue): MiddlewareQueue
    {
        // Add your middlewares here

        return $middlewareQueue;
    }

    /**
     * Returns a service provider instance.
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request Request
     * @return \Authentication\AuthenticationServiceInterface
     */
    public function getAuthenticationService(ServerRequestInterface $request): AuthenticationServiceInterface
    {
        $service = new AuthenticationService();

        $fields = [
            'username' => 'username',
            'password' => 'password',
        ];
        $service->loadAuthenticator('Authentication.Jwt', [
            'returnPayload' => false,
            'queryParam' => 'token',
        ]);
        $service->loadAuthenticator('Authentication.Form', [
            'fields' => $fields,
        ]);
        $service->loadIdentifier('Authentication.JwtSubject', [
            'tokenField' => 'id',
            'dataField' => 'id',
        ]);
        $service->loadIdentifier('Authentication.Password', [
            'returnPayload' => false,
            'fields' => $fields,
        ]);

        return $service;
    }
}
