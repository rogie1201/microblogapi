<?php
namespace Api\Model\Entity;

use Cake\ORM\Entity;

class Like extends Entity
{
    // Make all fields mass assignable except for primary key field "id".
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
