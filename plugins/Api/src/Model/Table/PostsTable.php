<?php

namespace Api\Model\Table;

use Cake\Datasource\ConnectionManager;
use Cake\ORM\Table;
use Cake\Validation\Validator;

//use Cake\ORM\Query;

class PostsTable extends Table
{
    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->scalar('content')
            ->requirePresence('content', 'create')
            ->notEmptyString('content', 'Please fill out this field.');

        $validator
            ->scalar('user_id')
            ->requirePresence('user_id', 'create')
            ->notEmptyString('user_id', 'Please fill out this field.');

        // $validator
        //     ->allowEmptyFile('image')
        //     ->add('image', [
        //         'mimeType' => [
        //             'rule' => ['mimeType', ['image/jpg', 'image/png', 'image/jpeg', 'image/gif']],
        //             'message' => 'Please upload only jpg, png, gif.',
        //         ],
        //         'fileSize' => [
        //             'rule' => ['fileSize', '<=', '2MB'],
        //             'message' => 'Image file size must be less than 2MB.',
        //         ],
        //     ]);

        return $validator;
    }

    /**
     * Query Feed.
     *
     * @param int $userId The user id.
     *
     * @return array of posts
     */
    public function getFeed($userId)
    {
        $connection = ConnectionManager::get('default');
        $results = $connection
            ->newQuery()
            ->select('*')
            ->from('post_views')
            ->where(['GROUP_ID' => $userId])
            ->order(['MODIFIED' => 'DESC'])
            ->execute()
            ->fetchAll('assoc');

        return $results;
    }

    /**
     * Database view that query all the posts that user posted.
     *
     * @param int $userId The user id.
     *
     * @return array An array of posts.
     */
    public function getFeedPostPerUser($userId)
    {
        $connection = ConnectionManager::get('default');
        $results = $connection
            ->newQuery()
            ->select('*')
            ->from('user_post_views')
            ->where(['GROUP_ID' => $userId])
            ->order(['MODIFIED' => 'DESC'])
            ->execute()
            ->fetchAll('assoc');

        return $results;
    }
}
