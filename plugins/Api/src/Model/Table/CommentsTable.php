<?php

namespace Api\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

//use Cake\ORM\Query;

class CommentsTable extends Table
{
    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->scalar('post_id')
            ->requirePresence('post_id', 'create')
            ->notEmptyString('post_id', 'Please fill out this field.');

        $validator
            ->scalar('user_id')
            ->requirePresence('user_id', 'create')
            ->notEmptyString('user_id', 'Please fill out this field.');

        $validator
            ->scalar('comment_text')
            ->requirePresence('comment_text', 'create')
            ->notEmptyString('comment_text', 'Please fill out this field.');

        return $validator;
    }
}
