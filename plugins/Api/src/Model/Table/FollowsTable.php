<?php

namespace Api\Model\Table;

use Cake\Datasource\ConnectionManager;
use Cake\ORM\Table;
use Cake\Validation\Validator;

//use Cake\ORM\Query;

class FollowsTable extends Table
{
    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {

        $validator
            ->scalar('user_id')
            ->requirePresence('user_id', 'create')
            ->notEmptyString('user_id', 'Please fill out this field.');

        $validator
            ->scalar('follower_user_id ')
            ->requirePresence('follower_user_id', 'create')
            ->notEmptyString('follower_user_id', 'Please fill out this field.');

        return $validator;
    }

    /**
     * Query all the users who is being followed by this user.
     *
     * @param int $userId The user id.
     *
     * @return array An array of users.
     */
    public function getFollowings($userId)
    {
        $connection = ConnectionManager::get('default');
        $results = $connection
            ->newQuery()
            ->select(['b.full_name',
                'b.username',
                'b.id as user_id',
                'b.image',
                'a.created',
                'a.id as follow_id'])
            ->from('follows as a')
            ->join([
                'b' => [
                    'table' => 'users',
                    'type' => 'INNER',
                    'conditions' => 'a.user_id = b.id',
                ]])
            ->where(['a.follower_user_id = ' . $userId . '', 'a.is_deleted = 0',
                'b.is_deleted = 0', 'b.id != ' . $userId . ''])
            ->execute()
            ->fetchAll('assoc');

        return $results;
    }

    /**
     * Query all the users who is following this user.
     *
     * @param int $userId The user id.
     *
     * @return array An array of users.
     */
    public function getFollowers($userId)
    {
        $connection = ConnectionManager::get('default');
        $results = $connection
            ->newQuery()
            ->select(['b.full_name',
                'b.username',
                'b.id as user_id',
                'b.image',
                'a.created',
                'a.id as follow_id'])
            ->from('follows as a')
            ->join([
                'b' => [
                    'table' => 'users',
                    'type' => 'INNER',
                    'conditions' => 'a.follower_user_id = b.id',
                ]])
            ->where(['a.user_id = ' . $userId . '', 'a.is_deleted = 0', 'b.is_deleted = 0', 'b.id != ' . $userId . ''])
            ->execute()
            ->fetchAll('assoc');

        return $results;
    }
}
