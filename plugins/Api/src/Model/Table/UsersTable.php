<?php

namespace Api\Model\Table;

use Cake\Auth\DefaultPasswordHasher;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class UsersTable extends Table
{
    // public function initialize(array $config): void
    // {
    //     parent::initialize($config);
    //     $this->table('User');
    // }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->scalar('full_name')
            ->requirePresence('full_name', 'create')
            ->notEmptyString('full_name', 'Please fill out this field.');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmptyString('email', 'Please fill out this field.');

        $validator
            ->scalar('birthdate')
            ->requirePresence('birthdate', 'create')
            ->notEmptyString('birthdate', 'Please fill out this field.');

        $validator
            ->scalar('username')
            ->requirePresence('username', 'create')
            ->notEmptyString('username', 'Please fill out this field.');

        $validator
            ->scalar('password')
            ->maxLength('password', 250)
            ->requirePresence('password', 'create')
            ->notEmptyString('password', 'Please fill out this field.');

        $validator
            ->add('birthdate', 'checkDOB', [
                'rule' => function ($value, $context) {
                    if ($value == date('Y-m-d')
                        ||
                        $value > date('Y-m-d')) {

                        return 'The date of birth is not valid';
                    }
                    return true;
                },
            ]);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param string $email
     * @return boolean if email is available
     */
    public function isEmailAvailable($email)
    {
        $query = $this->find('all')
            ->where(
                [
                    'Users.email' => $email,
                ]
            )
            ->first();
        if (empty($query)) {
            return true;
        }
        return false;
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationLoginApi(Validator $validator): Validator
    {
        $validator
            ->email('email', 'Please provide a valid email address.')
            ->requirePresence('email', 'create', 'This is required parameter.')
            ->notEmpty('email', 'Email address is required');

        $validator
            ->requirePresence('password', 'create', 'This is required parameter.')
            ->notEmpty('password', 'Password is required.');

        $validator
        ->allowEmptyFile('image')
        ->add('image', [
            'mimeType' => [
                'rule' => ['mimeType', ['image/jpg', 'image/png', 'image/jpeg', 'image/gif']],
                'message' => 'Please upload only jpg, png, gif.',
            ],
            'fileSize' => [
                'rule' => ['fileSize', '<=', '2MB'],
                'message' => 'Image file size must be less than 2MB.',
            ],
        ]);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['email'], 'The email address is already in use. Please use a different email address.'));
        $rules->add($rules->isUnique(['username'], 'The username is unavailable. Please use a different username.'));

        return $rules;
    }

    /**
     * Hash the password.
     *
     * @param $value.
     * @return boolean true
     */
    public function getPassword($value)
    {
        if (strlen($value)) {
            $hasher = new DefaultPasswordHasher();

            return $hasher->hash(strval($value));
        }
    }

    public function isValid($token)
    {
        $user_data = $this->find('all')
            ->where(
                [
                    'api_token' => $token,
                ]
            )
            ->first();
        return $user_data;
    }

    /**
     * Method that will check if birth date is valid.
     *
     * @return boolean true(If it is available) or false(It is unavailable)
     */
    public function checkDOB()
    {
        if ($this->data[$this->alias]['birthdate'] == date('Y-m-d') ||
            $this->data[$this->alias]['birthdate'] > date('Y-m-d')) {
            return false;
        }
        return true;
    }

    /**
     * Method that will get all the users that is not followed by the current user.
     *
     * @param int $user_id The user id.
     *
     * @return array List of users.
     */
    public function getUserToFollow($userId)
    {
        $connection = ConnectionManager::get('default');
        $followings = $connection
            ->newQuery()
            ->select('user_id')
            ->from('follows')
            ->where(['follower_user_id' => $userId, 'is_deleted' => 0])
            ->execute()
            ->fetchAll('assoc');

        $arrayFollowings = array();
        for ($i = 0; $i < count($followings); $i++) {
            array_push($arrayFollowings, $followings[$i]['user_id']);
        }
        $List = implode(', ', $arrayFollowings); // implode convert array to string exmple (10,20,30)
        $getUserToFollow = $connection
            ->newQuery()
            ->select('*')
            ->from('users')
            ->where(['id !=' => $userId, 'is_deleted' => 0, 'id NOT IN' => '(' . $List . ')'])
            ->order(['created' => 'DESC'])
            ->limit(5)
            ->execute()
            ->fetchAll('assoc');

        return $getUserToFollow;
    }

    /**
     * Change passwor.
     *
     * @param $userId.
     * @param $newPassword.
     * @return void
     */
    public function changePassword($userId, $newPassword)
    {
        $connection = ConnectionManager::get('default');
        $connection->update('users', ['password' => $newPassword], ['id' => $userId]);
    }

    /**
     * Hash the password for digest auth.
     *
     * @param $event.
     * @return boolean true
     */
    // public function beforeSave(EventInterface $event)
    // {
    //     $entity = $event->getData('entity');

    //     // Make a password for digest auth.
    //     $entity->digest_hash = DigestAuthenticate::password(
    //         $entity->username,
    //         $entity->plain_password,
    //         env('SERVER_NAME')
    //     );
    //     return true;
    // }
}