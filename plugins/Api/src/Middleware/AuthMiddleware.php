<?php namespace API\Middleware;

/**
 * Client Secret Filter Middleware
 *
 * This middlware reads out the Client-Secret header
 * sent by each request if it matches the server's.
 */
class AuthMiddleware
{
    /**
     * Invoke
     *
     * Gets directly called by the Middleware runner.
     *
     * @param \Cake\Http\Request $request
     * @param \Cake\Http\Response $response
     * @param $next
     * @return empty|\Cake\Http\Response
     */
    public function __invoke($request, $response, $next)
    {
        if (!$request->hasHeader('Client-Secret')) {
            return $this->unauthorized($response);
        }
        $client_secret = $request->getHeaderLine('Client-Secret');
        if ($client_secret != env('APP_CLIENT_SECRET')) {
            return $this->unauthorized($response);
        }
        $response = $next($request, $response);
        return $response;
    }
    /**
     * Gets called by this middleware when the Client Secret
     * is invalid.
     *
     * This function writes the 401 status code on the response object
     * and puts approriate message back to the caller.
     *
     * This function can be directly returned.
     *
     * @param \Cake\Http\Response $response
     * @return \Cake\Http\Response $response
     */
    public function unauthorized($response)
    {
        $response->statusCode(401);
        $response->body(
            json_encode(
                [
                    'status' => 'client_unauthorized',
                    'message' => 'This client is unauthorized to access this server. ',
                ]
            )
        );
        return $response;
    }
}
