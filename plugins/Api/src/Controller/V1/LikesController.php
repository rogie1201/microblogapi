<?php

namespace Api\Controller\V1;

use Api\Controller\AppController;

class LikesController extends AppController
{
    /**
     * Like a post.
     *
     * @return Json message, HTTP Status Codes
     */
    public function like()
    {
        $this->request->allowMethod(['post', 'put']);
        $likeData = $this->request->getData();
        $acceptHeader = getallheaders();
        $validUser = $this->Authentication->checkAuth($acceptHeader);
        $code = 401;
        $message = 'Unauthorized';
        $isValid = false;
        if (isset($validUser)) {
            $like = $this->Likes->newEntity($this->request->getData());
            if (isset($likeData['post_id']) && isset($likeData['user_id'])) {
                $likeGet = $this->Likes->find('all')
                    ->where(
                        [
                            'post_id' => $likeData['post_id'],
                            'user_id' => $validUser['id'],
                        ]
                    )
                    ->first();
                $isValid = $likeData['user_id'] == $validUser['id'] ? true : false;
            }
            if ($isValid) {
                if (!empty($likeGet)) {
                    $likeGet['is_deleted'] = 0;
                    $this->Likes->save($likeGet);
                    $message = 'Post Successfully Liked.';
                    $code = 200;
                } else {
                    if ($this->Likes->save($like)) {
                        $code = 200;
                        $message = 'Post Successfully Liked.';
                    } else {
                        $message = $like->getErrors();
                        $code = 400;
                    }
                }
            }
        }
        $this->set([
            'message' => $message,
            'code' => $code,
        ]);
        $this->response = $this->response->withStatus(intval($code));
        $this->viewBuilder()->setOption('serialize', ['message', 'code']);
    }

    /**
     * Unlike a post.
     *
     * @return Json message, HTTP Status Codes
     */
    public function unlike()
    {
        $this->request->allowMethod(['patch', 'put', 'post']);
        $likeData = $this->request->getData();
        $acceptHeader = getallheaders();
        $validUser = $this->Authentication->checkAuth($acceptHeader);
        $code = 401;
        $message = 'Unauthorized';
        $isValid = false;
        if (isset($validUser)) {
            $like = $this->Likes->newEntity($this->request->getData());
            if (isset($likeData['post_id']) && isset($likeData['user_id'])) {
                $likeGet = $this->Likes->find('all')
                    ->where(
                        [
                            'post_id' => $likeData['post_id'],
                            'user_id' => $validUser['id'],
                        ]
                    )
                    ->first();
                $isValid = $likeData['user_id'] == $validUser['id'] ? true : false;
            }
            if ($isValid) {
                if (!empty($likeGet)) {
                    $likeGet['is_deleted'] = 1;
                    $this->Likes->save($likeGet);
                    $message = 'Post Successfully unliked.';
                    $code = 200;
                } else {
                    $message = $like->getErrors();
                    $code = 400;
                }
            }
        }
        $this->set([
            'message' => $message,
            'code' => $code,
        ]);
        $this->response = $this->response->withStatus(intval($code));
        $this->viewBuilder()->setOption('serialize', ['message', 'code']);
    }

    /**
     * View number of likes in a post.
     *
     * @param int $id The post id
     *
     * @return Json message, HTTP Status Codes
     */
    public function view($id)
    {
        $this->request->allowMethod(['get']);
        $acceptHeader = getallheaders();
        $validUser = $this->Authentication->checkAuth($acceptHeader);
        $code = 401;
        $message = 'Unauthorized';
        $likesCount = array();
        if (isset($validUser)) {
            $likesCount = $this->Likes->find('all')
                ->where(
                    [
                        'post_id' => $id,
                        'is_deleted' => 0,
                    ]
                )
                ->all();
            if (count($likesCount) <= 0) {
                $message = 'Post not found';
                $code = 404;
            } else {
                $message = 'Success';
                $code = 200;
            }
        }
        $this->set([
            'message' => $message,
            'code' => $code,
            'likes' => count($likesCount),
        ]);
        $this->response = $this->response->withStatus(intval($code));
        $this->viewBuilder()->setOption('serialize', ['likes', 'message', 'code']);
    }
}
