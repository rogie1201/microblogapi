<?php

namespace Api\Controller\V1;

use Api\Controller\AppController;

class CommentsController extends AppController
{
    /**
     * Add a comment.
     *
     * @return Json message, HTTP Status Codes
     */
    public function comment()
    {
        $this->request->allowMethod(['post', 'put']);
        $request = $this->request->getData();
        $acceptHeader = getallheaders();
        $validUser = $this->Authentication->checkAuthRequest($request, $acceptHeader);
        if (isset($validUser)) {
            $commentContent = array(
                'post_id' => $request['post_id'],
                'user_id' => $validUser['id'],
                'comment_text' => $request['comment_text'],
            );
            $comment = $this->Comments->newEntity($commentContent);
            if ($this->Comments->save($comment)) {
                $code = 200;
                $message = 'Success';
            } else {
                $message = $comment->getErrors();
                $code = 400;
            }
        } else {
            $code = 401;
            $message = 'Unauthorized';
        }
        $this->set([
            'message' => $message,
            'code' => $code,
        ]);
        $this->response = $this->response->withStatus(intval($code));
        $this->viewBuilder()->setOption('serialize', ['message', 'code']);
    }

    /**
     * View comments of a post.
     *
     * @param int $id The post id
     *
     * @return Json message, HTTP Status Codes
     */
    public function view($id)
    {
        $this->request->allowMethod(['get']);
        $acceptHeader = getallheaders();
        $validUser = $this->Authentication->checkAuth($acceptHeader);
        if (isset($validUser)) {
            $comment = $this->Comments->find('all')
                ->where(
                    [
                        'post_id' => $id,
                        'is_deleted' => 0,
                    ]
                )
                ->all();
            if (count($comment) <= 0) {
                $message = 'Post not found';
                $code = 404;
            } else {
                $message = 'Success';
                $code = 200;
            }
        } else {
            $comment = [];
            $code = 401;
            $message = 'Unauthorized';
        }

        $this->set([
            'message' => $message,
            'code' => $code,
            'Comments' => $comment,
        ]);
        $this->response = $this->response->withStatus(intval($code));
        $this->viewBuilder()->setOption('serialize', ['Comments', 'message', 'code']);
    }

    /**
     * Delete a comment.
     *
     * @return Json message, HTTP Status Codes
     */
    public function delete($id)
    {
        $this->request->allowMethod(['delete']);
        $acceptHeader = getallheaders();
        $validUser = $this->Authentication->checkAuth($acceptHeader);
        $code = 401;
        $message = 'Unauthorized';
        if (isset($validUser)) {
            $isUser = $this->Comments->find('all')
                ->where(
                    [
                        'id' => $id,
                        'user_id' => $validUser['id'],
                    ]
                )
                ->count();
            if ($isUser > 0) {
                $comment = $this->Comments->get($id);
                if (!$this->Comments->delete($comment)) {
                    $message = 'Request failed';
                    $code = 400;
                } else {
                    $message = 'Successfully Deleted';
                    $code = 200;
                }
            }
        }

        $this->set([
            'message' => $message,
            'code' => $code,
        ]);
        $this->response = $this->response->withStatus(intval($code));
        $this->viewBuilder()->setOption('serialize', ['message', 'code']);
    }
}
