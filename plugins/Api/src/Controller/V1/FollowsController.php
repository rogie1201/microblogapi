<?php

namespace Api\Controller\V1;

use Api\Controller\AppController;

class FollowsController extends AppController
{
    /**
     * Follow user.
     *
     * @return Json message, HTTP Status Codes
     */
    public function follow()
    {
        $this->request->allowMethod(['post', 'put']);
        $followData = $this->request->getData();
        $acceptHeader = getallheaders();
        $validUser = $this->Authentication->checkAuth($acceptHeader);
        $code = 401;
        $message = 'Unauthorized';
        $isValid = false;
        if (isset($validUser)) {
            $follow = $this->Follows->newEntity($this->request->getData());
            if (isset($followData['user_id']) && isset($followData['follower_user_id'])) {
                $followGet = $this->Follows->find('all')
                    ->where(
                        [
                            'user_id' => $followData['user_id'],
                            'follower_user_id' => $validUser['id'],
                        ]
                    )
                    ->first();
                $isValid = $followData['follower_user_id'] == $validUser['id'] ? true : false;
            }
            if ($isValid) {
                if (!empty($followGet)) {
                    $followGet['is_deleted'] = 0;
                    $this->Follows->save($followGet);
                    $message = 'Successfully Followed';
                    $code = 200;
                } else {
                    if ($this->Follows->save($follow)) {
                        $code = 200;
                        $message = 'Successfully Followed';
                    } else {
                        $message = $follow->getErrors();
                        $code = 400;
                    }
                }
            }
        }
        $this->set([
            'message' => $message,
            'code' => $code,
        ]);
        $this->response = $this->response->withStatus(intval($code));
        $this->viewBuilder()->setOption('serialize', ['message', 'code']);
    }

    /**
     * Unfollow a user.
     *
     * @return Json message, HTTP Status Codes
     */
    public function unfollow()
    {
        $this->request->allowMethod(['post', 'put']);
        $followData = $this->request->getData();
        $acceptHeader = getallheaders();
        $validUser = $this->Authentication->checkAuth($acceptHeader);
        $code = 401;
        $message = 'Unauthorized';
        $isValid = false;
        if (isset($validUser)) {
            $follow = $this->Follows->newEntity($this->request->getData());
            if (isset($followData['user_id']) && isset($followData['follower_user_id'])) {
                $followGet = $this->Follows->find('all')
                    ->where(
                        [
                            'user_id' => $followData['user_id'],
                            'follower_user_id' => $validUser['id'],
                        ]
                    )
                    ->first();
                $isValid = $followData['follower_user_id'] == $validUser['id'] ? true : false;
            }
            if ($isValid) {
                if (!empty($followGet)) {
                    $followGet['is_deleted'] = 1;
                    $this->Follows->save($followGet);
                    $message = 'Successfully unfollowed';
                    $code = 200;
                } else {
                    $message = $follow->getErrors();
                    $code = 400;
                }
            }
        }
        $this->set([
            'message' => $message,
            'code' => $code,
        ]);
        $this->response = $this->response->withStatus(intval($code));
        $this->viewBuilder()->setOption('serialize', ['message', 'code']);
    }

    /**
     * View number of followers of a user.
     *
     * @param int $id The user id
     *
     * @return Json message, HTTP Status Codes
     */
    public function followers($id)
    {
        $this->request->allowMethod(['get']);
        $acceptHeader = getallheaders();
        $validUser = $this->Authentication->checkAuth($acceptHeader);
        $code = 401;
        $message = 'Unauthorized';
        $follow = array();
        if (isset($validUser)) {
            $follow = $this->Follows->find('all')
                ->where(
                    [
                        'user_id' => $id,
                        'is_deleted' => 0,
                    ]
                )
                ->all();
            if (count($follow) <= 0) {
                $message = 'User not found';
                $code = 404;
            } else {
                $message = 'Success';
                $code = 200;
            }
        }
        $this->set([
            'message' => $message,
            'code' => $code,
            'Followers' => count($follow),
        ]);
        $this->response = $this->response->withStatus(intval($code));
        $this->viewBuilder()->setOption('serialize', ['Followers', 'message', 'code']);
    }

    /**
     * View number of followings of a user.
     *
     * @param int $id The user id
     *
     * @return Json message, HTTP Status Codes
     */
    public function followings($id)
    {
        $this->request->allowMethod(['get']);
        $acceptHeader = getallheaders();
        $validUser = $this->Authentication->checkAuth($acceptHeader);
        $code = 401;
        $message = 'Unauthorized';
        $follow = array();
        if (isset($validUser)) {
            $follow = $this->Follows->find('all')
                ->where(
                    [
                        'follower_user_id' => $id,
                        'is_deleted' => 0,
                    ]
                )
                ->all();
            if (count($follow) <= 0) {
                $message = 'User not found';
                $code = 404;
            } else {
                $message = 'Success';
                $code = 200;
            }
        }
        $this->set([
            'message' => $message,
            'code' => $code,
            'Following' => count($follow),
        ]);
        $this->response = $this->response->withStatus(intval($code));
        $this->viewBuilder()->setOption('serialize', ['Following', 'message', 'code']);
    }
}
