<?php

namespace Api\Controller\V1;

use Api\Controller\AppController;
use Api\Model\Table\FollowsTable;
use Api\Model\Table\PostsTable;
use Cake\Auth\DefaultPasswordHasher;
use Cake\HTTP\Response;
use Cake\I18n\FrozenTime;
use Cake\Mailer\Mailer;
use Cake\Routing\Router;

class UsersController extends AppController
{
    /**
     * Get all users.
     *
     * @return Json list of users
     */
    public function index()
    {
        $this->request->allowMethod(['get', 'post']);
        //$request = $this->request->getData();
        $acceptHeader = getallheaders();
        if (isset($acceptHeader['Authorization'])) {
            $userData = $this->Authentication->isValid($acceptHeader['Authorization']);
        }
        if (isset($userData)) {
            $users = $userData;
            $postTable = new PostsTable();
            $followTable = new FollowsTable();
            $posts = $postTable->getFeedPostPerUser($userData['id']);
            $followers = $followTable->getFollowers($userData['id']);
            $followings = $followTable->getFollowings($userData['id']);
            $tofollow = $this->Users->getUserToFollow($userData['id']);
            $code = 200;
            $message = 'success';
        } else {
            $users = [];
            $posts = [];
            $followers = [];
            $followings = [];
            $tofollow = [];
            $code = 401;
            $message = 'Unauthorized';
        }

        $this->response = $this->response->withStatus($code);
        $this->set([
            'users' => $users, 'posts' => $posts, 'followers' => $followers, 'followings' => $followings,
            'tofollow' => $tofollow, 'message' => $message, 'code' => $code]);
        $this->viewBuilder()->setOption('serialize', ['users', 'posts', 'followers', 'followings',
            'tofollow', 'message', 'code']);

        //print_r($acceptHeader);
    }

    /**
     * Register new user.
     *
     * @return Json user, message, HTTP Status Codes
     */
    public function add()
    {
        $this->request->allowMethod(['post', 'put']);
        $user = $this->Users->newEntity($this->request->getData());
        $activation_key = time();
        $user['code_of_activation'] = $activation_key;
        $url = Router::url(['controller' => 'users', 'action' => 'activate']);
        if ($this->Users->save($user)) {
            $mailer = new Mailer('default');
            $mailer
                ->setEmailFormat('html')
                ->setTo($user['email'])
                ->setSubject('Account Activation')
                ->setFrom(['microblog1201@gmail.com' => 'Microblog'])
                ->setViewVars(['activationUrl' => $activation_key]) //'http://' . env('SERVER_NAME') . $url . '/' . $activation_key . '.json'])
                ->viewBuilder()
                ->setTemplate('activation')
                ->setLayout('activation');
            if (!$mailer->deliver()) {
                $code = 400;
            } else {
                $code = 200;
            }
            $message = 'Successfully registered. Activation code has been sent to your email.';
        } else {
            $message = $user->getErrors();
            $code = 409;
        }

        $this->response = $this->response->withStatus(intval($code));
        $this->set([
            'message' => $message,
            'code' => $code,
            'user' => $user,
        ]);
        $this->viewBuilder()->setOption('serialize', ['message', 'code']);
    }

    /**
     * Activate a user account.
     *
     * @param int $activation_key The key to activate the user
     *
     * @return Json user, message, HTTP Status Codes
     */
    public function activate($activation_key)
    {
        $this->request->allowMethod(['patch', 'post', 'put']);
        $userData = $this->Users->find('all')
            ->where(
                [
                    'code_of_activation' => $activation_key,
                    'email_activated IS NULL',
                ]
            )
            ->first();

        if (!empty($userData)) {
            $userData['code_of_activation'] = '-';
            $userData['email_activated'] = FrozenTime::now();
            $userData['image'] = 'default-profile.jpg';
            $this->Users->save($userData);
            $message = 'Your account has been activated. You may now login to access your account.';
            $code = 200;
        } else {
            $message = 'Invalid request.';
            $code = 400;
        }

        $this->response = $this->response->withStatus(intval($code));
        $this->set([
            'message' => $message,
            'code' => $code,
            'user' => $userData,
        ]);
        $this->viewBuilder()->setOption('serialize', ['message', 'code']);
    }

    /**
     * View specific User.
     *
     * @param int $username The username
     *
     * @return Json user, message, HTTP Status Codes
     */
    public function view($id)
    {
        $this->request->allowMethod(['get']);
        $acceptHeader = getallheaders();
        $validUser = $this->Authentication->checkAuth($acceptHeader);
        if (isset($validUser)) {
            $user = $this->Users->find('all')
                ->where(
                    [
                        'id' => $id,
                        'email_activated IS NOT NULL',
                    ]
                )
                ->first();
            if (empty($user)) {
                $message = 'User not found';
                $code = 404;
            } else {
                $message = 'Success';
                $code = 200;
            }
        } else {
            $user = [];
            $code = 401;
            $message = 'Unauthorized';
        }
        $this->response = $this->response->withStatus(intval($code));
        $this->set([
            'message' => $message,
            'code' => $code,
            'user' => $user,
        ]);
        $this->viewBuilder()->setOption('serialize', ['user', 'message', 'code']);
    }

    /**
     * Edit specific User.
     *
     * @param int $id The user id
     *
     * @return Json user, message, HTTP Status Codes
     */
    public function edit()
    {
        $this->request->allowMethod(['patch', 'post', 'put']);
        $acceptHeader = getallheaders();
        $validUser = $this->Authentication->checkAuth($acceptHeader);
        $request = $this->request->getData();
        if (isset($validUser)) {
            $user = $this->Users->find('all')
                ->where(
                    [
                        'id' => $validUser['id'],
                        'email_activated IS NOT NULL',
                    ]
                )
                ->first();
            if (!empty($user)) {
                $userPass = true;
                if (!empty($request['currentPassword'])) {
                    $userPass = $this->getPassword($request['currentPassword'], $user['password']);
                }
                if ($userPass == false) {
                    $userdata = [];
                    $message = ['password' => ['noMatch' => 'The password you entered was incorrect.']];
                    $code = 409;
                } elseif (!empty($request['password']) && empty($request['currentPassword'])) {
                    $userdata = [];
                    $message = ['password' => ['noMatch' => 'Please enter current password.']];
                    $code = 409;
                } else {
                    $userdata = $this->Users->patchEntity($user, $this->request->getData());
                    if ($this->Users->save($userdata)) {
                        $message = 'success';
                        $code = 200;
                    } else {
                        $userdata = [];
                        $message = $userdata->getErrors(); //'Bad Request';
                        $code = 409;
                    }
                }
            } else {
                $message = 'User not found';
                $code = 404;
                $userdata = [];
            }
        } else {
            $userdata = [];
            $code = 401;
            $message = 'Unauthorized';
        }

        $this->response = $this->response->withStatus(intval($code));
        $this->set([
            'message' => $message,
            'code' => $code,
            'user' => $userdata,
        ]);
        $this->viewBuilder()->setOption('serialize', ['user', 'message', 'code']);
    }

    /**
     * Function to  login user
     * Create token
     *
     * @return Json user, message, HTTP Status Codes. Token
     */
    public function login()
    {
        $this->request->allowMethod(['post']);
        $request = $this->request->getData();
        if (isset($request['username']) && isset($request['password'])) {
            $user = $this->Users->find('all')
                ->where(
                    [
                        'username' => $request['username'],
                    ]
                )
                ->first();
            if (isset($user)) {
                $userPass = $this->getPassword($request['password'], $user['password']);
            }
        }
        if (isset($user) && $userPass == true) {
            $access_token = $this->Token->generate(32);
            $requestData = array(
                'api_token' => $access_token,
                'token_expire' => date(
                    'Y-m-d H:i:s',
                    strtotime(date('Y-m-d H:i:s') . ' +1 days')
                ),
            );
            $userdata = $this->Users->patchEntity($user, $requestData);
            if ($this->Users->save($userdata)) {
                $message = 'Success';
                $code = 200;
            } else {
                $message = 'Bad Request';
                $code = 400;
                $access_token = null;
            }
        } else {
            $message = 'Invalid username or password';
            $code = 401;
            $userdata = [];
            $access_token = null;
            $user = [];
        }
        $this->response = $this->response->withStatus(intval($code));
        $this->set([
            'user' => $user,
            'message' => $message,
            'code' => $code,
            'hash' => $access_token,
        ]);
        $this->viewBuilder()->setOption('serialize', ['hash', 'user', 'message', 'code']);
    }

    /**
     * Get current password
     *
     * @param $inputPassword The password
     * @param $user The current password
     *
     * @return password hash
     */
    public function getPassword($inputPassword, $user)
    {
        if (strlen($inputPassword) > 0) {
            $hasher = new DefaultPasswordHasher();

            return $hasher->check($inputPassword, $user);
        }
    }
}