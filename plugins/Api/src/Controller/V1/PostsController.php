<?php

namespace Api\Controller\V1;

use Api\Controller\AppController;

class PostsController extends AppController
{
    /**
     * Get all posts.
     *
     * @return array list of posts
     */
    public function index()
    {
        $this->request->allowMethod(['get']);
        $acceptHeader = getallheaders();
        $validUser = $this->Authentication->checkAuth($acceptHeader);
        if (isset($validUser)) {
            // $posts = $this->Posts->find('all', [
            //     'order' => ['created' => 'DESC']])
            //     ->where(
            //         [
            //             'user_id' => $validUser['id'],
            //             'is_deleted' => 0,
            //         ]
            //     )->all();
            $posts = $this->Posts->getFeed($validUser['id']);

            $code = 200;
            $message = 'success';
        } else {
            $posts = [];
            $code = 401;
            $message = 'Unauthorized';
        }

        $this->set([
            'posts' => $posts,
            'message' => $message,
            'code' => $code,
        ]);
        $this->response = $this->response->withStatus($code);
        $this->viewBuilder()->setOption('serialize', ['posts', 'message', 'code']);
    }

    /**
     * Add new Post.
     *
     * @return Json user, message, HTTP Status Codes
     */
    public function add()
    {
        $this->request->allowMethod(['post', 'put']);
        $request = $this->request->getData();
        $acceptHeader = getallheaders();
        $validUser = $this->Authentication->checkAuth($acceptHeader);
        $post = [];
        $code = 401;
        $message = 'Unauthorized';
        $isValid = false;
        $postContent = array();
        if (isset($validUser)) {
            if (isset($request['user_id'])) {

                //     if (!empty($request['image'])) {
                //         $postData = [
                //             'user_id' => $session->read('user.id'),
                //             'content' => $request['post_txt'],
                //             'image' => $request['image']];
                //     } else {
                //         $postData = [
                //             'user_id' => $session->read('user.id'),
                //             'content' => $request['post_txt'],
                //         ];
                //     }

                // $postContent = array(
                //     'user_id' => $validUser['id'],
                //     'content' => $request['content'],
                //     'image' => $attachment->getClientFilename(),
                // );
                $isValid = $request['user_id'] == $validUser['id'] ? true : false;
            }
            if ($isValid) {
                $post = $this->Posts->newEntity($request);
                if ($this->Posts->save($post)) {
                    $code = 200;
                    $message = 'Successfully posted.';
                } else {
                    $message = $post->getErrors();
                    $code = 400;
                }
            }
        }
        $this->set([
            'message' => $message,
            'code' => $code,
            'post' => $post,
        ]);
        $this->response = $this->response->withStatus(intval($code));
        $this->viewBuilder()->setOption('serialize', ['post', 'message', 'code']);
    }

    /**
     * Edit specific User.
     *
     * @param int $id The post id
     *
     * @return Json user, message, HTTP Status Codes
     */
    public function edit($id)
    {
        $this->request->allowMethod(['post', 'put']);
        $acceptHeader = getallheaders();
        $request = $this->request->getData();
        $validUser = $this->Authentication->checkAuth($acceptHeader);
        $message = 'Unauthorized';
        $code = 401;
        $postData = [];
        $isValid = false;
        if (isset($validUser)) {
            $posts = $this->Posts->find('all')
                ->where(
                    [
                        'id' => $id,
                        'user_id' => $validUser['id'],
                    ]
                )
                ->first();
            if (isset($request['user_id'])) {
                $isValid = $request['user_id'] == $validUser['id'] ? true : false;
            }
            if ($isValid) {
                if (!empty($posts)) {
                    $postData = $this->Posts->patchEntity($posts, $request);
                    if ($this->Posts->save($postData)) {
                        $message = 'Success';
                        $code = 200;
                    } else {
                        $message = 'Bad Request';
                        $code = 400;
                    }
                } else {
                    $message = 'Post not found';
                    $code = 404;
                    $postData = [];
                }
            }
        }
        $this->set([
            'message' => $message,
            'code' => $code,
            'post' => $postData,
        ]);
        $this->response = $this->response->withStatus(intval($code));
        $this->viewBuilder()->setOption('serialize', ['post', 'message', 'code']);
    }

    /**
     * View specific Post.
     *
     * @param int $id The post id
     *
     * @return Json Post, message, HTTP Status Codes
     */
    public function view($id)
    {
        $this->request->allowMethod(['get']);
        $acceptHeader = getallheaders();
        $validUser = $this->Authentication->checkAuth($acceptHeader);
        $code = 401;
        $message = 'Unauthorized';
        $post = array();
        if (isset($validUser)) {
            $post = $this->Posts->find('all')
                ->where(
                    [
                        'id' => $id,
                        'is_deleted' => 0,
                    ]
                )
                ->first();
            if (empty($post)) {
                $message = 'Post not found';
                $code = 404;
            } else {
                $message = 'Success';
                $code = 200;
            }
        }
        $this->set([
            'message' => $message,
            'code' => $code,
            'post' => $post,
        ]);
        $this->response = $this->response->withStatus(intval($code));
        $this->viewBuilder()->setOption('serialize', ['post', 'message', 'code']);
    }

    /**
     * Delete Post.
     *
     * @param int $id The post id
     *
     * @return Json Post, message, HTTP Status Codes
     */
    public function delete($id)
    {
        $this->request->allowMethod(['delete']);
        $acceptHeader = getallheaders();
        $validUser = $this->Authentication->checkAuth($acceptHeader);
        $code = 401;
        $message = 'Unauthorized';
        if (isset($validUser)) {
            $isUser = $this->Posts->find('all')
                ->where(
                    [
                        'id' => $id,
                        'user_id' => $validUser['id'],
                    ]
                )
                ->count();
            if ($isUser > 0) {
                $post = $this->Posts->get($id);
                if (!empty($post)) {
                    $post['is_deleted'] = 1;
                    $this->Posts->save($post);
                    $message = 'Post successfully deleted.';
                    $code = 200;
                } else {
                    $message = 'Invalid request.';
                    $code = 400;
                }
            }
        }
        $this->set([
            'message' => $message,
            'code' => $code,
        ]);
        $this->response = $this->response->withStatus(intval($code));
        $this->viewBuilder()->setOption('serialize', ['message', 'code']);
    }

    /**
     * Share a Post.
     *
     * @param int $id The post id
     *
     * @return Json message, HTTP Status Codes
     */
    public function share($id)
    {
        $this->request->allowMethod(['post', 'put']);
        $UserData = $this->request->getData();
        $acceptHeader = getallheaders();
        $validUser = $this->Authentication->checkAuth($acceptHeader);
        $code = 401;
        $message = 'Unauthorized';
        if (isset($validUser)) {
            $this->request->allowMethod(['post', 'put']);
            $getContent = $this->Posts->find('all')
                ->where(
                    [
                        'id' => $id,
                    ]
                )
                ->first();
            $share = array(
                'repost_id' => $id,
                'user_id' => $validUser['id'],
                'content' => $getContent['content'],
                'image' => $getContent['image'],
            );
            if (!empty($getContent)) {
                $post = $this->Posts->newEntity($share);
                if ($this->Posts->save($post)) {
                    $code = 200;
                    $message = 'Successfully Shared.';
                } else {
                    $message = $post->getErrors();
                    $code = 400;
                }
            } else {
                $message = 'Post not Found';
                $code = 404;
            }
        }
        $this->set([
            'message' => $message,
            'code' => $code,
        ]);
        $this->response = $this->response->withStatus(intval($code));
        $this->viewBuilder()->setOption('serialize', ['message', 'code']);
    }
}
