<?php

namespace Api\Controller;

use Cake\Controller\Controller;

class AppController extends Controller
{
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('FormProtection');`
     *
     * @return void
     */
    public function initialize(): void
    {
        parent::initialize();
        // Load Request handler
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');

        $this->loadComponent(
            'Authentication',
            [
                'className' => '\Api\Controller\Component\AuthenticationComponent',
            ]
        );

        $this->loadComponent(
            'Token',
            [
                'className' => '\Api\Controller\Component\TokenComponent',
            ]
        );
    }
}
