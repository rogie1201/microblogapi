<?php

namespace Api\Controller\Component;

use Api\Model\Table\UsersTable;
use Cake\Controller\Component;

/**
 * Authentication
 *
 * The authentication component handles the logic needed when
 * authenticating clients with API.
 */
class AuthenticationComponent extends Component
{
    /**
     * Check valid user.
     *
     * @param string $token
     * @return App\Model\Entity\User
     */
    public function isValid($token)
    {
        $user = new UsersTable();
        $user_data = $user->find('all')
            ->where(
                [
                    'api_token' => $token,
                    'token_expire >' => date('Y-m-d H:i:s'),
                ]
            )
            ->first();

        return $user_data;
    }

    /**
     * Check valid login user with request data.
     *
     * @param array $request
     * @param array $acceptHeader
     * @return App\Model\Entity\User
     */
    public function checkAuthRequest($request, $acceptHeader)
    {
        $isValid = false;
        if (isset($acceptHeader['Authorization']) && isset($request['user_id'])) {
            $userData = $this->isValid($acceptHeader['Authorization']);
            $isValid = $request['user_id'] == $userData['id'] ? true : false;
        }
        $res = null;
        if (isset($userData) && $isValid) {
            $res = $userData;
        }
        return $res;
    }

    /**
     * Check valid login user.
     *
     * @param array $acceptHeader
     * @return App\Model\Entity\User
     */
    public function checkAuth($acceptHeader)
    {
        if (isset($acceptHeader['Authorization'])) {
            $userData = $this->isValid($acceptHeader['Authorization']);
        }
        $res = null;
        if (isset($userData)) {
            $res = $userData;
        }
        return $res;
    }
}
