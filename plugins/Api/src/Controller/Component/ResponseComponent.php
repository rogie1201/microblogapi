<?php

namespace Api\Controller\Component;

use Cake\Controller\Component;
use Cake\HTTP\Response;

/**
 * Response Component contains the default API
 * response format.
 */
class ResponseComponent extends Component
{
    /**
     * Initializes the component.
     *
     * @param array $config
     * @return void
     */
    public function initialize(array $config):void
    {
        parent::initialize($config);
    }

    /**
     * A request success response.
     *
     * @param Cake\Http\Request $request
     * @param string $status The status code to return.
     * @param array|null|object $result The result object you want to return.
     * @param string $message Optional message to the client.
     * @return Cake\Http\Request Returns a request with a buffered response body.
     */
    public function success(
        $request,
        $status = "success",
        $result = null,
        $message = "The request was successful."
    ) {
        $request->getgetStatusCode(200);
        $request->getBody(
            json_encode(
                [
                    'status' => $status,
                    'message' => $message,
                    'result' => $result,
                ]
            )
        );
        return $request;
    }
    /**
     * A request failed response.
     *
     * @param Cake\Http\Request $request
     * @param string $status The status code to return.
     * @param array|null|object $result The result object you want to return.
     * @param string $message Optional message to the client.
     * @return Cake\Http\Request Returns a request with a buffered response body.
     */
    public function failed(
        $request,
        $status = "failed",
        $result = null,
        $message = "The request failed."
    ) {
        $request->getStatusCode(400);
        $request->getBody(
            json_encode(
                [
                    'status' => $status,
                    'message' => $message,
                    'result' => $result,
                ]
            )
        );
        return $request;
    }
    /**
     * A request forbidden response.
     *
     * @param Cake\Http\Request $request
     * @param string $status The status code to return.
     * @param array|null|object $result The result object you want to return.
     * @param string $message Optional message to the client.
     * @return Cake\Http\Request Returns a request with a buffered response body.
     */
    public function forbidden(
        $request,
        $status = "forbidden",
        $result = null,
        $message = "The request was forbidden."
    ) {
        $request->getStatusCode(403);
        $request->getBody(
            json_encode(
                [
                    'status' => $status,
                    'message' => $message,
                    'result' => $result,
                ]
            )
        );
        return $request;
    }
    /**
     * A request unauthorized response.
     *
     * @param Cake\Http\Request $request
     * @param string $status The status code to return.
     * @param array|null|object $result The result object you want to return.
     * @param string $message Optional message to the client.
     * @return Cake\Http\Request Returns a request with a buffered response body.
     */
    public function unauthorized(
        $request,
        $status = "unauthorized",
        $result = null,
        $message = "You are unauthorized to access this resource. Please login first."
    ) {
        $request->setStatusCode(401);
        $request->setBody(
            json_encode(
                [
                    'status' => $status,
                    'message' => $message,
                    'result' => $result,
                ]
            )
        );
        return $request;
    }
    /**
     * A request authorized response.
     *
     * @param Cake\Http\Request $request
     * @param string $status The status code to return.
     * @param array|null|object $result The result object you want to return.
     * @param string $message Optional message to the client.
     * @return Cake\Http\Request Returns a request with a buffered response body.
     */
    public function authorized(
        $request,
        $status = "authorized",
        $result = null,
        $message = "You are authorized to access the server."
    ) {
        $request->getStatusCode(402);
        $request->getBody(
            json_encode(
                [
                    'status' => $status,
                    'message' => $message,
                    'result' => $result,
                ]
            )
        );
        return $request;
    }
    /**
     * An internal error response.
     *
     * @param Cake\Http\Request $request
     * @param string $status The status code to return.
     * @param array|null|object $result The result object you want to return.
     * @param string $message Optional message to the client.
     * @return Cake\Http\Request Returns a request with a buffered response body.
     */
    public function internalError(
        $request,
        $status = "internal_error",
        $result = null,
        $message = "An error has occurred."
    ) {
        $request->getStatusCode(500);
        $request->getBody(
            json_encode(
                [
                    'status' => $status,
                    'message' => $message,
                    'result' => $result,
                ]
            )
        );
        return $request;
    }
    /**
     * A custom response.
     *
     * @param Cake\Http\Request $request
     * @param string $status The status code to return.
     * @param array|null|object $result The result object you want to return.
     * @param string $message Optional message to the client.
     * @return Cake\Http\Request Returns a request with a buffered response body.
     */
    public function response(
        $request,
        $httpCode,
        $status,
        $result,
        $message
    ) {
        $request->getStatusCode(200);
        $request->getBody(
            json_encode(
                [
                    'status' => $status,
                    'message' => $message,
                    'result' => $result,
                ]
            )
        );
        return $request;
    }
}
