<div class="" id="" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <section>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-8">
                                <div class="row">
                                    <div class="col text-center">
                                        <h1>Activate account</h1>
                                    </div>
                                </div>
                                <?=$this->Flash->render() ?>
                                <!-- <form name="myform" action="<?php //echo $this->Url->build('/users/login', ['fullBase' => true]) ?>" method="post"> -->
                                <!-- <?=$this->Form->create() ?> -->
                                <?php

                                echo $this->Form->create(null, [
                                    'url' => [
                                        'controller' => 'users',
                                        'action' => 'activate',
                                    ],
                                    'method' => 'post',
                                ]
                                );

                                ?>
                                <div class="row align-items-center">
                                    <div class="col mt-4">
                                        <input type="text" class="form-control" placeholder="Enter activation code" name="code">
                                    </div>
                                </div>

                                <div class="row align-items-center mt-4">
                                    <div class="col">

                                        <?php echo $this->Form->button(__('Submit'), ['class' => 'form-control btn btn-primary', 'type' => 'submit', 'id' => 'btn_sigin']); ?>

                                    </div>
                                </div>

                                <?=$this->Form->end() ?>

                                <center>
                                    <div class="row justify-content-start mt-4">
                                        <div class="col">
                                            <div class="form-check">
                                                <label class="form-check-label">

                                                    <?=$this->Html->link("Sign in",
                                                    array(
                                                        'controller' => 'users',
                                                        'action' => 'login',
                                                    )) ?>
                                                    to Microblog
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </center>
                            </div>
                        </div>
                    </div>
                </section>



            </div>
        </div>
    </div>
</div>