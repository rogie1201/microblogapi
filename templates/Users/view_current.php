<div class="" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Profile</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php
                        echo $this->Form->create(null, [
                            'url' => [
                                'controller' => 'users',
                                'action' => 'edit',
                            ],
                            'enctype' => 'multipart/form-data',
                        ]
                        );
                            //print_r($this->request->getSession()->read('errors'));
                        $errors = $this->request->getSession()->consume('errors');
                    ?>

                <div class="card-body">

                    <div class="row">
                        <div class="col-12 col-sm-auto mb-3">
                            <div class="mx-auto" style="width: 140px;">
                                <div class="d-flex justify-content-center align-items-center rounded"
                                    style="height: 140px; background-color: rgb(233, 236, 239);">
                                    <img src="<?php echo $this->Url->build('/img/' . $userData['image']);?>"
                                        class="img-responsive img-fluid" id="imgPost">
                                </div>
                            </div>
                        </div>
                        <div class="col d-flex flex-column flex-sm-row justify-content-between mb-3">
                            <div class="text-center text-sm-left mb-2 mb-sm-0">
                                <h4 class="pt-sm-2 pb-1 mb-0 text-nowrap">
                                    <?php echo $userData['full_name']; ?></h4>
                                <p class="mb-0">@<?php echo $userData['username']; ?></p>
                                <div class="text-muted">
                                    <small><?php echo $userData['email']; ?></small>
                                </div>
                                <div class="text-muted"><small>Born
                                        <?php echo $this->Time->format($userData['birthdate'], 'MMM dd, yyyy'); ?></small>
                                </div>
                                <div class="mt-2">
                                    <label class="btn btn-primary">
                                        <i class="fas fa-image"></i>
                                        Edit Photo<input type="file" id="imgInp" name="postImage" hidden>
                                    </label>
                                </div>

                            </div>
                            <div class="text-center text-sm-right">
                                <div class="text-muted"><small>Joined
                                        <?php echo $this->Time->format($userData['created'], 'MMM dd, yyyy'); ?></small>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-content pt-3">
                        <div class="tab-pane active">
                            <div class="row">
                                <div class="col">
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label>Name</label>
                                                <input class="form-control" type="text" name="full_name"
                                                    value="<?php echo $userData['full_name']; ?>">
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group">
                                                <label>Username</label>
                                                <input class="form-control" type="text" name="username"
                                                    value="<?php echo $userData['username']; ?>">
                                                <?php if (isset($errors['username'])): ?>
                                                <div class="alert alert-danger alert-dismissible">
                                                    <button type="button" class="close" data-dismiss="alert"
                                                        aria-hidden="true">×</button>
                                                    <?php
                                                        if (isset($errors['username']['_isUnique'])) {
                                                            echo h($errors['username']['_isUnique']);
                                                        }
                                                        ?>
                                                </div>
                                                <?php endif ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input name="email" class="form-control" type="text"
                                                    value="<?php echo $userData['email']; ?>">
                                                <?php if (isset($errors['email'])): ?>
                                                <div class="alert alert-danger alert-dismissible">
                                                    <button type="button" class="close" data-dismiss="alert"
                                                        aria-hidden="true">×</button>
                                                    <?php
                                                        if (isset($errors['email']['_isUnique'])) {
                                                            echo h($errors['email']['_isUnique']);
                                                        }
                                                        ?>
                                                </div>
                                                <?php endif ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-sm-6 mb-3">
                                    <div class="mb-2"><b>Change Password</b></div>
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label>Current Password</label>
                                                <input class="form-control" type="password" placeholder="••••••"
                                                    name="currentPassword">
                                                <?php if (isset($errors['password']['noMatch'])): ?>
                                                <div class="alert alert-danger alert-dismissible">
                                                    <button type="button" class="close" data-dismiss="alert"
                                                        aria-hidden="true">×</button>
                                                    <?php
                                                            echo h($errors['password']['noMatch']);
                                                        ?>
                                                </div>
                                                <?php endif ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label>New Password</label>
                                                <input class="form-control" type="password" placeholder="••••••"
                                                    name="newPassword" id="newPassword" onkeyup="pass1();">
                                                <?php if (isset($errors['password']['_empty'])): ?>
                                                <div class="alert alert-danger alert-dismissible">
                                                    <button type="button" class="close" data-dismiss="alert"
                                                        aria-hidden="true">×</button>
                                                    <?php
                                                            echo h($errors['password']['_empty']);
                                                        ?>
                                                </div>
                                                <?php endif ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label>Confirm <span class="d-none d-xl-inline">Password</span></label>
                                                <input class="form-control" type="password" placeholder="••••••"
                                                    name="ConfirmNewPassword" id="ConfirmNewPassword"
                                                    onkeyup="pass2();">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="alert alert-warning alert-dismissible" id="error_password"
                                        style="display:none;">
                                        <?php echo "The password you entered did not match."; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <?php
                        echo $this->Html->link('Back',
                            array(
                                'controller' => 'posts',
                                'action' => 'index',
                            ),
                            array(
                                'bootstrap-type' => 'primary',
                                'class' => 'btn btn-secondary',
                                'data-dismiss' => 'modal',
                                // transform link to a button
                                'rule' => 'button',
                            )
                        );
                        ?>

                    <button type="submit" class="btn btn-primary">Save changes</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
    $("#removeImgFile").click(function() {
        $("#imgPost").val(null);
        $('#imgPost').attr('src', "#");
        $("#imgPost").hide();
        $("#removeImgFile").hide();
        $("#imgInp").val("");
    });


    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#imgPost').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);

            $("#imgPost").show();
            $("#removeImgFile").show();
        }
    }

    $("#imgInp").change(function() {
        readURL(this);
    });


    function pass1() {
        if ($('#ConfirmNewPassword').val() != "") {
            if ($('#ConfirmNewPassword').val() != $('#newPassword').val()) {
                $("#error_password").show();
                $(':input[type="submit"]').prop('disabled', true);
            } else {
                $("#error_password").hide();
                $(':input[type="submit"]').prop('disabled', false);
            }
        }
    }

    function pass2() {
        if ($('#newPassword').val() != $('#ConfirmNewPassword').val()) {
            $("#error_password").show();
            $(':input[type="submit"]').prop('disabled', true);
        } else {
            $("#error_password").hide();
            $(':input[type="submit"]').prop('disabled', false);
        }
    }
    </script>