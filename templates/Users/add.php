<section>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-8 col-lg-8 col-xl-6">
                <div class="row">
                    <div class="col text-center">
                        <h1>Create your account</h1>
                        <p class="text-h3">You are creating an account to a simple microblog.</p>
                    </div>
                </div>

                <?=$this->Flash->render() ?>

                 <?php

                    echo $this->Form->create(null, [
                        'url' => [
                            'controller' => 'users',
                            'action' => 'add',
                        ],
                        'method' => 'post',
                    ]
                    );

                    ?>
                <div class="row align-items-center">
                    <div class="col mt-4">
                        <label for="full_name" class="control-label">Name*</label>
                        <input type="text" class="form-control" placeholder="Name" name="full_name"
                            value="<?=(isset($data['full_name'])) ? h($data['full_name']) : ''; ?>">
                    </div>
                </div>
                <?php if (isset($errors['full_name'])): ?>
                <div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?=h($errors['full_name']['_empty']) ?>
                </div>
                <?php endif ?>

                <div class="row align-items-center mt-4">
                    <div class="col">
                        <label for="email" class="control-label">Email*</label>
                        <input type="email" class="form-control" placeholder="Email" name="email"
                            value="<?=(isset($data['email'])) ? h($data['email']) : ''; ?>">
                    </div>
                </div>
                <?php if (isset($errors['email'])): ?>
                <div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php

                    if (isset($errors['email']['_empty'])) {
                        echo h($errors['email']['_empty']);
                    }
                    if (isset($errors['email']['_isUnique'])) {
                        echo h($errors['email']['_isUnique']);
                    }
                    ?>
                 </div>
                <?php endif ?>

                <div class="row align-items-center mt-4">
                    <div class="col">
                        <label for="birthDate" class="control-label">Date of Birth*</label>
                        <input type="date" class="form-control" placeholder="Birthdate" name="birthdate"
                            title="Birthdate" value="<?=(isset($data['birthdate'])) ? h($data['birthdate']) : ''; ?>">
                    </div>
                </div>
                <?php if (isset($errors['birthdate'])): ?>
                <div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php

                        if (isset($errors['birthdate']['_empty'])) {
                            echo h($errors['birthdate']['_empty']);
                        }
                        if (isset($errors['birthdate']['checkDOB'])) {
                            echo h($errors['birthdate']['checkDOB']);
                        }
                        ?>
                </div>
                <?php endif ?>

                <div class="row align-items-center">
                    <div class="col mt-4">
                        <label for="username" class="control-label">Username*</label>
                        <input type="text" class="form-control" placeholder="Username" name="username"
                            value="<?=(isset($data['username'])) ? h($data['username']) : ''; ?>">
                    </div>
                </div>
                <?php if (isset($errors['username'])): ?>
                <div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php

if (isset($errors['username']['_empty'])) {
    echo h($errors['username']['_empty']);
}
if (isset($errors['username']['_isUnique'])) {
    echo h($errors['username']['_isUnique']);
}
?>
                </div>
                <?php endif ?>


                <div class="row align-items-center mt-4">
                    <div class="col">
                        <label for="username" class="control-label">Password*</label>
                        <input type="password" class="form-control" placeholder="Password" name="password"
                            id="password1" value="<?=(isset($data['password'])) ? h($data['password']) : ''; ?>">
                    </div>
                    <div class="col">
                        <label for="username" class="control-label">Confirm Password*</label>
                        <input type="password" class="form-control" placeholder="Confirm Password"
                        id="password2" value="<?=(isset($data['password'])) ? h($data['password']) : ''; ?>">
                    </div>
                </div>
                <?php if (isset($errors['password'])): ?>
                <div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?=h($errors['password']['_empty']) ?>
                </div>
                <?php endif ?>


                <div class="alert alert-warning alert-dismissible" id="error_password" style="display:none;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo "The password you entered did not match."; ?>
                </div>

                <div class="row align-items-center mt-4">
                    <div class="col">

                        <?php echo $this->Form->button(__('Sign up'), ['class' => 'form-control btn btn-primary', 'type' => 'submit', 'id' => 'btn_signup']); ?>

                    </div>
                </div>

                <?php //echo $this->Form->end(); ?>
                </form>
                <center>
                    <div class="row justify-content-start mt-4">
                        <div class="col">
                            <div class="form-check">
                                <label class="form-check-label">
                                    Already have an account?
                                    <?=$this->Html->link("Sign in",
    array(
        'controller' => 'users',
        'action' => 'login',
    )) ?>
                                    to Microblog
                                </label>
                            </div>
                        </div>
                    </div>
                    <center>
            </div>
        </div>
    </div>
</section>


<script>
// $(document).ready(function()
//   {
//       $("#error_password").hide();
//   });

$('#password2').on('keyup', function() {
    if ($('#password1').val() != this.value) {
        $("#error_password").show();
        $(':input[type="submit"]').prop('disabled', true);
    } else {
        $("#error_password").hide();
        $(':input[type="submit"]').prop('disabled', false);
    }
});

$('#password1').on('keyup', function() {
    if ($('#password2').val() != "") {
        if ($('#password2').val() != this.value) {
            $("#error_password").show();
            $(':input[type="submit"]').prop('disabled', true);
        } else {
            $("#error_password").hide();
            $(':input[type="submit"]').prop('disabled', false);
        }
    }

    baseUrl();


});
</script>