<div class="card card-primary">

    <!-- form start -->
    <!-- <form method="post" action="/posts/add" enctype="multipart/form-data"> -->
         <?php

        echo $this->Form->create(null, [
            'url' => [
                'controller' => 'posts',
                'action' => 'add',
            ],
            'enctype' => 'multipart/form-data',
        ]
        );

        ?>
    <div class="card-body">
        <div class="form-group">

            <textarea class="form-control form-control-lg" rows="2" name="post_txt"
                placeholder="What's happening?"></textarea>
        </div>

        <div class="card-footer">
            <div class="float-right">
                <!-- <button type="submit" class="btn btn-primary"><i class="far fa-write"></i>
                Post</button> -->
                <?php echo $this->Form->button(__('Post'), ['class' => 'form-control btn btn-primary', 'type' => 'submit', 'id' => 'btn_post']); ?>

            </div>
            <label class="btn btn-default">
                <i class="fas fa-image"></i>
                Insert Image <input type="file" id="imgInp" name="postImage" hidden>
            </label>
            <br>
            <br>

            <div class="feed-image p-2 px-3">
                <img id="imgPost" src="#" class="img-fluid res" style="display:none;" />
                <a class="remove-image" id="removeImgFile" href="#" style="display: none;">&#215;</a>
            </div>


        </div>
        <?=$this->Form->end() ?>
        <!-- form end -->
    </div>

    <script>
    $("#removeImgFile").click(function() {
        $("#imgPost").val(null);
        $('#imgPost').attr('src', "#");
        $("#imgPost").hide();
        $("#removeImgFile").hide();
        $("#imgInp").val("");
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#imgPost').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);

            $("#imgPost").show();
            $("#removeImgFile").show();
        }
    }

    $("#imgInp").change(function() {
        readURL(this);
    });
    </script>