var themes = [
    'cerulean',
    'cosmo',
    'cyborg',
    'darkly',
    'flatly',
    'journal',
    'litera',
    'lumen',
    'lux',
    'materia',
    'minty',
    'pulse',
    'sandstone',
    'simplex',
    'sketchy',
    'slate',
    'solar',
    'spacelab',
    'superhero',
    'united',
    'yeti'
];

$(document).ready(function() {
    $('[data-class]').click(function() {
        updateNavbarClass($(this).attr('data-class'));
    });

    //updateNavbarClass('fixed-left');

    themes.forEach(function(theme) {
        $('#theme_select').append($('<option>', {
            value: theme,
            text: theme.charAt(0).toUpperCase() + theme.slice(1),
            selected: theme === 'materia'
        }));
    });
});

function updateNavbarClass(className) {
    $('nav')
        .removeClass(function(index, css) {
            return (css.match(/(^|\s)fixed-\S+/g) || []).join(' ');
        })
        .addClass(className);

    $('[data-class]').removeClass('active').parent('li').removeClass('active');
    $('[data-class="' + className + '"]').addClass('active').parent('li').addClass('active');

    fixBodyMargin(className);
}

function fixBodyMargin(className) {
    if (/fixed-(left|right)/.test(className)) {
        $('body').removeAttr('style');
        if (className === 'fixed-right') {
            $('body').css('marginLeft', 0);
        } else {
            $('body').css('marginRight', 0);
        }
    } else {
        $('body').css({
            "margin-right": 0,
            "margin-left": 0,
            "padding-top": '90px'
        });
    }
}

function selectTheme(theme) {
    $('#theme_link').attr('href', 'https://cdnjs.cloudflare.com/ajax/libs/bootswatch/4.3.1/' + theme + '/bootstrap.min.css');
}


function like(post_id) {
    $.ajax({
        url: server_path() + '/likes/like',
        data: { post_id: post_id },
        datatype: 'json',
        type: 'post',
        success: function(response) {
            var result = response == 0 ? "" : response;
            $('#post_like_count_' + post_id).html(result);
            $('#post_like_' + post_id).addClass("display_none");
            $('#post_unlike_' + post_id).removeClass("display_none");

        }
    })
}

function unlike(post_id) {
    $.ajax({
        url: server_path() + '/likes/unlike',
        data: { post_id: post_id },
        datatype: 'json',
        type: 'post',
        success: function(response) {
            var result = response == 0 ? "" : response;
            $('#post_like_count_' + post_id).html(result);
            $('#post_like_' + post_id).removeClass("display_none");
            $('#post_unlike_' + post_id).addClass("display_none");
        }
    })
}

function addComment(post_id) {
    var post_text = $("#post_txt_" + post_id).val();
    $.ajax({
        url: server_path() + '/comments/addComment',
        data: {
            post_id: post_id,
            comment_text: post_text,
        },
        datatype: 'json',
        type: 'post',
        success: function(response) {
            $("#post_txt_" + post_id).val("");
            response = JSON.parse(response);
            var commentCount = response.comment_counts == 0 ? "" : response.comment_counts;
            $("#post_comment_count_" + post_id).text(commentCount);

            $("#commentDiv_" + post_id).prepend('<div id="commentLogDiv_' + response.comment_id + '"> ' +
                '<div class="comment-widgets border-bottom" id="commentDiv">' +
                '<div class="d-flex flex-row comment-row m-t-0">' +
                '<div class="p-2"><img src="' + server_path() + '/img/' + response.image + '" alt="user" width="50" class="rounded-circle">' +
                '</div>' +
                '<div class="comment-text w-100">' +
                '<div class="d-flex flex-column flex-wrap ml-2">' +
                '<a href="' + server_path() + '/users" class="font-weight-bold">' + response.full_name + '</a>' +
                '<span class="text-black-50 time">' + convertStringToDateTime(response.commentCreate) + '</span>' +
                '</div>' +
                '<span class="m-b-15 d-block">' + response.comment_text + '</span>' +
                '<div class="comment-footer">' +
                '<i class="fas fa-trash icon_gray" onclick="deleteComment(' + response.comment_id + ');">Delete</i>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>');
        }
    })
}

function deleteComment(id) {

    var result = confirm("Are you sure you want to delete this?");
    if (result) {
        $.ajax({
            url: server_path() + '/comments/deleteComment/' + id,
            data: { id: id },
            dataType: 'HTML',
            type: 'post',
            headers: { 'X-Requested-With': 'XMLHttpRequest' },
            success: function(response) {
                $("#commentLogDiv_" + id).hide();
                response = JSON.parse(response);
                var commentCount = response.comment_counts == 0 ? "" : response.comment_counts;
                $("#post_comment_count_" + response.post_id).text(commentCount);
            }
        });
    }
}

function convertStringToDateTime(dateTime) {
    var d = new Date(dateTime);
    var hours = d.getHours() % 12;
    hours = hours ? hours : 12;
    var thisDate = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'Decenber'][(d.getMonth())] + " " +
        ("00" + d.getDate()).slice(-2) + ", " +
        d.getFullYear() + " " +
        ("00" + hours).slice(-2) + ":" +
        ("00" + d.getMinutes()).slice(-2) + " " +
        (d.getHours() >= 12 ? 'PM' : 'AM');
    return thisDate;
}


function loadComment(id) {
    if ($('#divLoadComment_' + id).hasClass("displayed")) {
        $('#divLoadComment_' + id).toggle();
    }
    $.ajax({
        url: server_path() + '/posts/comment/' + id,
        data: { id: id },
        dataType: 'HTML',
        type: 'GET',
        headers: { 'X-Requested-With': 'XMLHttpRequest' },
        success: function(response) {
            $('#divLoadComment_' + id).html(response);
            $('#divLoadComment_' + id).addClass("displayed");
        }
    });
}

function loadUsersToFollow() {
    $("#rightNav").html('');
    $.ajax({
        url: server_path() + '/users/userToFollow/',
        dataType: 'HTML',
        type: 'GET',
        headers: { 'X-Requested-With': 'XMLHttpRequest' },
        success: function(response) {
            $("#rightNav").fadeOut("slow", function() {
                $('#rightNav').html(response);
                $('#rightNav').fadeIn("slow");
            })


        }
    });
}

//Load more data on scroll 
$(window).scroll(function() {
    if ($(window).scrollTop() == $(document).height() - $(window).height()) {
        // ajax call get data from server and append to the div
        alert("Load more data...");
    }
});