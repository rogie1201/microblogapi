<?php

namespace App\Model\Table;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class UsersTable extends Table
{
    // public function initialize(array $config): void
    // {
    //     parent::initialize($config);
    //     $this->table('User');
    // }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->scalar('full_name')
            ->requirePresence('full_name', 'create')
            ->notEmptyString('full_name', 'Please fill out this field.');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmptyString('email', 'Please fill out this field.');

        $validator
            ->scalar('birthdate')
            ->requirePresence('birthdate', 'create')
            ->notEmptyString('birthdate', 'Please fill out this field.');

        $validator
            ->scalar('username')
            ->requirePresence('username', 'create')
            ->notEmptyString('username', 'Please fill out this field.');

        $validator
            ->scalar('password')
            ->maxLength('password', 250)
            ->requirePresence('password', 'create')
            ->notEmptyString('password', 'Please fill out this field.');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param string $email
     * @return boolean if email is available
     */
    public function isEmailAvailable($email)
    {
        $query = $this->find('all')
            ->where(
                [
                    'Users.email' => $email,
                ]
            )
            ->first();
        if (empty($query)) {
            return true;
        }
        return false;
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationLoginApi(Validator $validator): Validator
    {
        $validator
            ->email('email', 'Please provide a valid email address.')
            ->requirePresence('email', 'create', 'This is required parameter.')
            ->notEmpty('email', 'Email address is required');

        $validator
            ->requirePresence('password', 'create', 'This is required parameter.')
            ->notEmpty('password', 'Password is required.');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['email'], 'The email address is already in use. Please use a different email address.'));
        $rules->add($rules->isUnique(['username'], 'The username is unavailable. Please use a different username.'));

        return $rules;
    }

    /**
     * Hash the password.
     *
     * @param $value.
     * @return boolean true
     */
    public function getPassword($value)
    {
        if (strlen($value)) {
            $hasher = new DefaultPasswordHasher();

            return $hasher->hash(strval($value));
        }
    }

    /**
     * Check valid token.
     *
     * @param $token.f
     * @return user data
     */
    public function isValid($token)
    {
        $user_data = $this->find('all')
            ->where(
                [
                    'api_token' => $token,
                ]
            )
            ->first();

        return $user_data;
    }

    /**
     * Hash the password for digest auth.
     *
     * @param $event.
     * @return boolean true
     */
    // public function beforeSave(EventInterface $event)
    // {
    //     $entity = $event->getData('entity');

    //     // Make a password for digest auth.
    //     $entity->digest_hash = DigestAuthenticate::password(
    //         $entity->username,
    //         $entity->plain_password,
    //         env('SERVER_NAME')
    //     );
    //     return true;
    // }
}