<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

//use Cake\ORM\Query;

class PostsTable extends Table
{
    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->scalar('content')
            ->requirePresence('content', 'create')
            ->notEmptyString('content', 'Please fill out this field.');

        $validator
            ->scalar('user_id')
            ->requirePresence('user_id', 'create')
            ->notEmptyString('user_id', 'Please fill out this field.');

        return $validator;
    }
}