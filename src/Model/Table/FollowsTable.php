<?php

namespace App\Model\Table;

use Cake\Datasource\ConnectionManager;
use Cake\ORM\Table;
use Cake\Validation\Validator;

//use Cake\ORM\Query;

class FollowsTable extends Table
{
    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {

        $validator
            ->scalar('user_id')
            ->requirePresence('user_id', 'create')
            ->notEmptyString('user_id', 'Please fill out this field.');

        $validator
            ->scalar('follower_user_id ')
            ->requirePresence('follower_user_id', 'create')
            ->notEmptyString('follower_user_id', 'Please fill out this field.');

        return $validator;
    }
}