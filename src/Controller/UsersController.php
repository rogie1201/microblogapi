<?php

namespace App\Controller;

use Cake\Http\Client;

class UsersController extends AppController
{

    /**
     * Display the details of the logged in user and its posts.
     *
     * @return array posts, followers, followings
     */
    public function index()
    {
        $apiUrl = $this->Authentication->apiUrl() . '/users/index.json';

        $session = $this->request->getSession();
        $hash = $session->read('hash');
        $http = new Client([
            'headers' => ['Authorization' => $hash],
        ]);

        $response = $http->get($apiUrl);

        $data = json_decode($response->getStringBody(), true);

        if ($data['code'] == 200) {
            $posts = $data['posts'];
            $this->set('posts', $posts);
            $this->set('followers', $data['followers']);
            $this->set('followings', $data['followings']);
            $this->set('tofollow', $data['tofollow']);
        } else {
            $auth = $this->Authentication->notValid($this->request);
            $this->Flash->error(__($auth[0]));
            return $this->redirect($auth[1]);
        }
    }

    /**
     * Function to  login user
     *
     * @return string user error exeption
     * @throws Exception If something goes wrong.
     */
    public function login()
    {
        $this->request->allowMethod(['post', 'get']);
        $request = $this->request->getData();
        if (!empty($request)) {
            $apiUrl = $this->Authentication->apiUrl() . '/users/login.json';
            $http = new Client();
            $response = $http->post($apiUrl, [
                'username' => $request['username'],
                'password' => $request['password'],
            ]);
            $data = json_decode($response->getStringBody(), true);
            $session = $this->request->getSession();
            $session->write('hash', $data['hash']);

            if ($data['code'] == 200) {
                $session->write('user', $data['user']);
                $redirect = $this->request->getQuery('redirect', [
                    'controller' => 'posts',
                    'action' => 'index',
                ]);

                return $this->redirect($redirect);
            } else {
                $this->Flash->error(__('Invalid username or password'));
            }
        }
    }

    /**
     * Add a user.
     *
     * Function will send email activation code to the user's email
     *
     * @return string user error exeption
     * @throws Exception If something goes wrong.
     */
    public function add()
    {
        $request = $this->request->getData();
        if (!empty($request)) {
            $apiUrl = $this->Authentication->apiUrl() . '/users/add.json';
            $http = new Client();
            $response = $http->post($apiUrl, [
                'full_name' => $request['full_name'],
                'email' => $request['email'],
                'birthdate' => $request['birthdate'],
                'username' => $request['username'],
                'password' => $request['password'],
            ]);
            $data = json_decode($response->getStringBody(), true);

            if ($data['code'] == 200) {
                $redirect = $this->request->getQuery('redirect', [
                    'controller' => 'users',
                    'action' => 'activate',
                ]);
                $this->Flash->success(__($data['message']));

                return $this->redirect($redirect);
            } else {
                $this->set(
                    'errors',
                    $data['message']
                );

                $this->set(
                    'data',
                    $request
                );
            }
        }
    }

    /**
     * Activate a user account.
     *
     * @return string user error exeption
     * @throws Exception If something goes wrong.
     */
    public function activate()
    {
        $request = $this->request->getData();
        if (!empty($request)) {
            $apiUrl = $this->Authentication->apiUrl() . '/users/activate/' . $request['code'] . '.json';
            $http = new Client();
            $response = $http->patch($apiUrl);
            $data = json_decode($response->getStringBody(), true);

            if ($data['code'] == 200) {
                $this->Flash->success(__($data['message']));
                $redirect = $this->request->getQuery('redirect', [
                    'controller' => 'users',
                    'action' => 'login',
                ]);

                return $this->redirect($redirect);
            } else {
                $this->Flash->error(__('Invalid request.'));
            }
        }
    }

    /**
     * Logout the user and clear his/her session.
     *
     * @return view to login page
     */
    public function logout()
    {
        $session = $this->request->getSession();
        $session->delete('hash');
        $session->delete('user');
        $redirect = $this->request->getQuery('redirect', [
            'controller' => 'users',
            'action' => 'login',
        ]);

        return $this->redirect($redirect);
    }

    /**
     * View the details of the user.
     *
     * @return array posts, followers, followings
     */
    public function viewCurrent()
    {
        $session = $this->request->getSession();
        $user = $session->read('user');
        if (!empty($user)) {
            $apiUrl = $this->Authentication->apiUrl() . '/users/view/' . $user['id'] . '.json';
            $session = $this->request->getSession();
            $hash = $session->read('hash');
            $http = new Client([
                'headers' => ['Authorization' => $hash],
            ]);
            $response = $http->get($apiUrl);
            $data = json_decode($response->getStringBody(), true);
            if ($data['code'] == 200) {
                $this->set('userData', $data['user']);
            } else {
                $this->set('errors', $data['message']);
            }
        } else {
            $auth = $this->Authentication->notValid($this->request);
            $this->Flash->error(__($auth[0]));

            return $this->redirect($auth[1]);
        }
    }

    /**
     * Edit a user account.
     *
     * @param int $id The user id
     *
     * @return string user error exeption
     * @throws Exception If something goes wrong.
     */
    public function edit()
    {
        $session = $this->request->getSession();
        $hash = $session->read('hash');
        $user = $session->read('user');
        $request = $this->request->getData();

        if (empty($user)) {
            return $this->redirect(array('action' => 'login'));
        }

        if (empty($request)) {
            return $this->redirect(array('action' => 'login'));
        }
        $apiUrl = $this->Authentication->apiUrl() . '/users/edit.json';
        $http = new Client([
            'headers' => ['Authorization' => $hash],
        ]);
        if (!empty($request['currentPassword']) || !empty($request['newPassword'])) {
            if (!empty($request['postImage']->getClientFilename())) {
                $fileName = $user['id'] . time() . '.' . $request['postImage']->getClientFilename();
                $userData = ['full_name' => $request['full_name'],
                    'email' => $request['email'],
                    'username' => $request['username'],
                    'currentPassword' => $request['currentPassword'],
                    'password' => $request['newPassword'],
                    'image' => $fileName];
            } else {
                $userData = ['full_name' => $request['full_name'],
                    'email' => $request['email'],
                    'username' => $request['username'],
                    'currentPassword' => $request['currentPassword'],
                    'password' => $request['newPassword']];
            }
        } elseif (!empty($request['postImage']->getClientFilename())) {
            $fileName = $user['id'] . time() . '.' . $request['postImage']->getClientFilename();
            $userData = ['full_name' => $request['full_name'],
                'email' => $request['email'],
                'username' => $request['username'],
                'image' => $fileName];
        } else {
            $userData = ['full_name' => $request['full_name'],
                'email' => $request['email'],
                'username' => $request['username']];
        }

        $response = $http->put($apiUrl, $userData);
        $data = json_decode($response->getStringBody(), true);

        if ($data['code'] == 200) {
            $this->Flash->success(__('Successfully changed.'));
            if (!empty($request['postImage']->getClientFilename())) {
                //$fileName = $user['id'] . time() . '.' . $request['postImage']->getClientFilename();
                $targetPath = WWW_ROOT . 'img' . DS . $fileName;
                if ($fileName) {
                    $request['postImage']->moveTo($targetPath);
                }
            }

            // debug($fileName);
            // exit;

            return $this->redirect(array('action' => 'viewCurrent'));
        } else {
            $session = $this->request->getSession();
            $session->write('errors', $data['message']);

            return $this->redirect(array('action' => 'viewCurrent'));
        }
    }

    /**
     * Get current password
     *
     * @param $inputPassword The password
     * @param $user The current password
     *
     * @return password hash
     */
    public function getPassword($inputPassword, $user)
    {
        if (strlen($inputPassword) > 0) {
            $hasher = new DefaultPasswordHasher();

            return $hasher->check($inputPassword, $user);
        }
    }

}