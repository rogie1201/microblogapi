<?php

namespace App\Controller;

use Cake\Http\Client;

class PostsController extends AppController
{
    /**
     * Get home feed posts
     *
     * @return string posts
     * @throws Exception If something goes wrong.
     */
    public function index()
    {
        $apiUrl = $this->Authentication->apiUrl() . '/posts/index.json';
        $session = $this->request->getSession();
        $hash = $session->read('hash');
        $http = new Client([
            'headers' => ['Authorization' => $hash],
        ]);
        $response = $http->get($apiUrl);
        $data = json_decode($response->getStringBody(), true);
        if ($data['code'] == 200) {
            $posts = $data['posts'];
            $this->set('posts', $posts);
        } else {
            $auth = $this->Authentication->notValid($this->request);
            $this->Flash->error(__($auth[0]));

            return $this->redirect($auth[1]);
        }
    }

    /**
     * Add a post with image(optional).
     *
     * @return void
     */
    public function add()
    {
        $session = $this->request->getSession();
        $request = $this->request->getData();
        $apiUrl = $this->Authentication->apiUrl() . '/posts/add.json';
        $session = $this->request->getSession();
        $hash = $session->read('hash');
        $http = new Client([
            'headers' => ['Authorization' => $hash],
        ]);
        if (empty($hash)) {
            $auth = $this->Authentication->notValid($this->request);
            $this->Flash->error(__($auth[0]));
            return $this->redirect(array('action' => 'login'));
        }
        if (strlen($request['post_txt']) == 0) {
            $this->Flash->error('Please input a description of your post.');

            return $this->redirect(array('action' => 'index'));
        }
        if (!empty($request['postImage']->getClientFilename())) {
            $fileName = $session->read('user.id') . time() . '.' . $request['postImage']->getClientFilename();
            $postData = [
                'user_id' => $session->read('user.id'),
                'content' => $request['post_txt'],
                'image' => $fileName];
        } else {
            $postData = [
                'user_id' => $session->read('user.id'),
                'content' => $request['post_txt'],
            ];
        }
        $response = $http->post($apiUrl, $postData);
        $data = json_decode($response->getStringBody(), true);
        if ($data['code'] == 200) {
            $this->Flash->success(__('Successfully posted.'));
            if (!empty($request['postImage']->getClientFilename())) {
                $targetPath = WWW_ROOT . 'img' . DS . $fileName;
                if ($fileName) {
                    $request['postImage']->moveTo($targetPath);
                }
            }

            return $this->redirect(array('action' => 'index'));
        } else {
            $this->Flash->error(__('Something went wrong.'));

            return $this->redirect(array('action' => 'index'));
        }
    }
}
