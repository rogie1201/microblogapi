<?php

namespace App\Controller\Component;

use Cake\Controller\Component;

/**
 * Authentication
 *
 * The authentication component handles the logic needed when
 * authenticating clients with API.
 */
class AuthenticationComponent extends Component
{

    /**
     * Redirect to login page.
     * @return login page
     */
    public function notValid($request)
    {
        $redirect = $request->getQuery('redirect', [
            'controller' => 'users',
            'action' => 'login',
        ]);
        $message = 'Do you think you are allowed to see that?';

        $arrData = array();

        array_push($arrData, $message);
        array_push($arrData, $redirect);

        return $arrData;
    }

    /**
     * Base URL of API.
     * @return API URL
     */
    public function apiUrl()
    {
        //$baseUrl = Router::url(['controller' => 'api/v1']);
        $baseUrl = '/microblogapi/api/v1';
        $apiUrl = 'http://' . env('SERVER_NAME') . $baseUrl;

        return $apiUrl;
    }
}